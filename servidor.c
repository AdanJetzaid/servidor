#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>

int main(void){
    struct sockaddr_in direccionServidor;
    direccionServidor.sin_family = AF_INET;
    direccionServidor.sin_addr.s_addr = INADDR_ANY;
    direccionServidor.sin_port = htons(8080);

    int servidor = socket(AF_INET, SOCK_STREAM, 0);

    int activado = 1;
    setsockopt(servidor, SOL_SOCKET, SO_REUSEADDR, &activado, sizeof(activado));

    if(bind(servidor, (void*) &direccionServidor, sizeof(direccionServidor)) != 0){
        perror("Fallo el bind");
        return 1;
    }
    printf("estoy escuchando\n");
    listen(servidor, 100);


    struct sockaddr_in direccionCliente;
    unsigned int tamanoDireccion;
    int cliente = accept(servidor, (void*)&direccionCliente, &tamanoDireccion);

    printf("recibi una conexion en %d!!\n", cliente);
    send(cliente, "Soy cliente, he recibido\n", 25, 0);
    send(cliente, "Adios", 5, 0);
    
    char* buffer = malloc(5);
    int bytesRecibidos = recv(cliente, buffer, 5, MSG_WAITALL);
    if(bytesRecibidos < 0){
        perror("Se desconecto");
        return 1;
    }
    buffer[bytesRecibidos] = '\0';
    printf("LLegaron %d bytes con %s", bytesRecibidos, buffer);

    free(buffer);
   
    return 0;

}
